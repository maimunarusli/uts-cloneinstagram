import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import ContentComp from './components/ContentComp'
import FooterComp from './components/FooterComp'
import HeaderComp from './components/HeaderComp'
import StorysComp from './components/StorysComp'
import Content1 from './components/Content1'
import Content2 from './components/Content2'
import Content3 from './components/Content3'
import Content4 from './components/Content4'


const App = () => {
  return (
    <View>
      <HeaderComp />
      <ScrollView style={{height:650,}}>
        <ScrollView horizontal>
          <StorysComp/>
        </ScrollView>
        <ContentComp />
       <Content1 />
       <Content2 />
       <Content3 />
       <Content4 />
      </ScrollView>
      <FooterComp />
    </View>
  )
}

export default App
