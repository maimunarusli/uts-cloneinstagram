import React, { Component } from 'react'
import { Image, Text, View } from 'react-native'

export default class ContentComp extends Component {
    render() {
        return (
            <View>
               <Image 
                source={require('../assets/tp.jpg')}
                style={{width:400,height:550,marginTop:10,marginBottom:60}}
               />
               <Image 
                source={require('../assets/iri.jpg')}
                style={{width:400,height:550,marginTop:5,marginBottom:60}}
               />
               <Image 
                source={require('../assets/uu.jpg')}
                style={{width:400,height:550,marginTop:5,marginBottom:60}}
               />
               <Image 
                source={require('../assets/bj.jpg')}
                style={{width:400,height:550,marginTop:5,marginBottom:60}}
               />
               <Image 
                source={require('../assets/uj.jpg')}
                style={{width:400,height:550,marginTop:5,marginBottom:60}}
               />

            </View>
            
        )
    }
}
