import React, { useState } from 'react'
import { View, Text, Button } from 'react-native'

const Hitung = () => {
    const [nomor, setNumber] = useState(0)
    return (
        <View>
            <Button title="tambah" onPress={()=> setNumber(nomor + 1)} />
            <Text>{nomor}</Text>
        </View>
    );
};

const StateComponent = () => {
    return(
        <View>
            <Text>Hello word</Text>
            <Hitung/>
        </View>
    );
};

export default StateComponent

