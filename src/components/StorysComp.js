import React, { Component } from 'react'
import { Image, Text, View } from 'react-native'

export default class StorysComp extends Component {
    render() {
        return (
            <View style={{backgroundColor:'#fff',flexDirection:'row'}}>
                <Image 
                    source={require('../assets/st.jpg')}
                    style={{width:70,height:70,borderRadius:35, borderWidth: 3,borderHeight: 10,borderColor:'grey',marginTop:10,marginLeft:20,marginBottom:5,}}
            
                />
                 <Image 
                    source={require('../assets/op.jpg')}
                    style={{width:70,height:70,borderRadius:35, borderWidth: 3,borderColor:'red',marginTop:10,marginLeft:15,marginBottom:5,}}   
                />
                 <Image 
                    source={require('../assets/pm.jpg')}
                    style={{width:70,height:70,borderRadius:35, borderWidth: 3,borderColor:'red',marginTop:10,marginLeft:15,marginBottom:5,}}
                    
                />
                 <Image 
                    source={require('../assets/pr.jpg')}
                    style={{width:70,height:70,borderRadius:35, borderWidth: 3,borderColor:'grey',marginTop:10,marginLeft:15,marginBottom:5,}}
                    
                />
                <Image 
                    source={require('../assets/ii.jpg')}
                    style={{width:70,height:70,borderRadius:35, borderWidth: 3,borderColor:'red',marginTop:10,marginLeft:15,marginBottom:5,}}
                    
                />

            </View>
        )
    }
}
